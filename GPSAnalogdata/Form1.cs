﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace GPSAnalogdata
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOKey_Click(object sender, EventArgs e)
        {
            Stopwatch watch = new Stopwatch();//代码执行时间
            watch.Start();
            double lat =Convert.ToDouble( txtLat.Text); 
            double lon =Convert.ToDouble( txtLon.Text);
            int num = Convert.ToInt32(txtNum.Text);
            //以上要做判断，省事没有写
            Random r = new Random(0);
            string initialDataFile = "..\\..\\UserIDLatLon.txt";
            FileStream ofs = new FileStream(initialDataFile, FileMode.Create);
            StreamWriter sw = new StreamWriter(ofs);
            for (int i = 0; i < num; ++i)
            {
                lat = (90.0 - (-90.0)) * r.NextDouble() + (-90.0);
                lon = (180.0 - (-180.0)) * r.NextDouble() + (-180.0);
                sw.WriteLine(i.ToString().PadLeft(6, '0') + "," + lat.ToString("F8") + "," + lon.ToString("F8") );
            }
            sw.Close();
            ofs.Close();
            watch.Stop();
            //获取当前实例测量得出的总运行时间（以毫秒为单位）
            string time = watch.ElapsedMilliseconds.ToString();
            MessageBox.Show("耗时：" + time + "毫秒，模拟数据生成完毕！");
        }
    }
}
