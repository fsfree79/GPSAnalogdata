﻿namespace GPSAnalogdata
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOKey = new System.Windows.Forms.Button();
            this.txtNum = new System.Windows.Forms.TextBox();
            this.labNum = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labLat = new System.Windows.Forms.Label();
            this.lablon = new System.Windows.Forms.Label();
            this.txtLon = new System.Windows.Forms.TextBox();
            this.txtLat = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOKey
            // 
            this.btnOKey.Location = new System.Drawing.Point(109, 172);
            this.btnOKey.Name = "btnOKey";
            this.btnOKey.Size = new System.Drawing.Size(75, 23);
            this.btnOKey.TabIndex = 0;
            this.btnOKey.Text = "生成";
            this.btnOKey.UseVisualStyleBackColor = true;
            this.btnOKey.Click += new System.EventHandler(this.btnOKey_Click);
            // 
            // txtNum
            // 
            this.txtNum.Location = new System.Drawing.Point(109, 130);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(100, 21);
            this.txtNum.TabIndex = 1;
            this.txtNum.Text = "1000000";
            // 
            // labNum
            // 
            this.labNum.AutoSize = true;
            this.labNum.Location = new System.Drawing.Point(56, 135);
            this.labNum.Name = "labNum";
            this.labNum.Size = new System.Drawing.Size(47, 12);
            this.labNum.TabIndex = 2;
            this.labNum.Text = "模拟量:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labLat);
            this.groupBox1.Controls.Add(this.lablon);
            this.groupBox1.Controls.Add(this.txtLon);
            this.groupBox1.Controls.Add(this.txtLat);
            this.groupBox1.Location = new System.Drawing.Point(49, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 81);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "GPS数据基点";
            // 
            // labLat
            // 
            this.labLat.AutoSize = true;
            this.labLat.Location = new System.Drawing.Point(7, 23);
            this.labLat.Name = "labLat";
            this.labLat.Size = new System.Drawing.Size(47, 12);
            this.labLat.TabIndex = 10;
            this.labLat.Text = "纬  度:";
            // 
            // lablon
            // 
            this.lablon.AutoSize = true;
            this.lablon.Location = new System.Drawing.Point(7, 54);
            this.lablon.Name = "lablon";
            this.lablon.Size = new System.Drawing.Size(47, 12);
            this.lablon.TabIndex = 9;
            this.lablon.Text = "经  度:";
            // 
            // txtLon
            // 
            this.txtLon.Location = new System.Drawing.Point(60, 51);
            this.txtLon.Name = "txtLon";
            this.txtLon.Size = new System.Drawing.Size(100, 21);
            this.txtLon.TabIndex = 8;
            this.txtLon.Text = "116.404269";
            // 
            // txtLat
            // 
            this.txtLat.Location = new System.Drawing.Point(60, 19);
            this.txtLat.Name = "txtLat";
            this.txtLat.Size = new System.Drawing.Size(100, 21);
            this.txtLat.TabIndex = 7;
            this.txtLat.Text = "39.916485";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 228);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labNum);
            this.Controls.Add(this.txtNum);
            this.Controls.Add(this.btnOKey);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOKey;
        private System.Windows.Forms.TextBox txtNum;
        private System.Windows.Forms.Label labNum;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labLat;
        private System.Windows.Forms.Label lablon;
        private System.Windows.Forms.TextBox txtLon;
        private System.Windows.Forms.TextBox txtLat;
    }
}

